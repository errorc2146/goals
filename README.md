This is a very very simple ToDo list and diary I'm writing to learn some Java.
You can set/delete goals, write entries, and browse through your history of goals/entries.

![Goals.png](https://bitbucket.org/repo/6BpREE/images/638032697-Goals.png)